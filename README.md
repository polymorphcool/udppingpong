# UDP ping pong

A super simple [godot](http://gotdotengine.org) project that shows how to use [PacketPeerUDP](https://docs.godotengine.org/en/3.1/classes/class_packetpeerudp.html) objects to send and receive UDP messages.

![](https://polymorph.cool/wp-content/uploads/2019/12/udppingpong_screenshot.png)

The application is sending the mouse position and prints the one received.

To test the project, you have to open godot 2 times. Once launched, click on "out"and "in" to start the emission and reception of messages. The "<>" button swaps the ports. Make sure you are not listening to the same port you are broadcasting on :)