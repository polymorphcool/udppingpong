extends Control

# network variables
var udp_out = null
var udp_in = null
var config_out = null
var config_in = null

# debug variables
var lines = []
var max_lines = 24
var lnum = 0

# others
var prev_mp = Vector2()

func network_in( in_port ):
	if int( in_port ) != config_in:
		if udp_in == null:
			udp_in = PacketPeerUDP.new()
		elif udp_in.is_listening():
			udp_in.close()
		config_in = int( in_port )
		var err = udp_in.listen( config_in, '0.0.0.0', 1024 )
		if ( err != OK ):
			print_line( "Unable to receive on: " + str(config_in) )
			config_in = null
		else:
			print_line( "Receiving on: " + str(config_in) )

func network_out( out_ip, out_port ):
	if out_ip != null and out_port != null:
		var p = str(out_ip) + ":" + str(out_port)
		if config_out != p:
			if udp_out == null:
				udp_out = PacketPeerUDP.new()
			else:
				udp_out.close()
			config_out = p
			udp_out.set_dest_address(str(out_ip), int(out_port))
			print_line( "Connecting to: " + config_out )

func parse_var( v ):
	if not v is Array:
		print_line( "message must be arrays" )
		return
	if len(v) == 0:
		print_line( "empty message" )
		return
	if v[0] == 'm':
		var v2 = v[1]
		print_line( v[0] + ": " + str(v2.x) + ", " + str(v2.y) )

func print_line( t ):
	lines.append( t )
	lnum += 1
	while len( lines ) > max_lines:
		lines.remove( 0 )
	$debug.text = ""
	var  i = 0
	for l in lines:
		$debug.text += str( lnum - len( lines ) + i ) + "|" + l + "\n"
		i += 1

func _ready():
	pass

func _process(delta):

	if udp_out != null and config_out != null:
		var mp = get_viewport().get_mouse_position()
		if prev_mp != mp:
			udp_out.put_var( [ 'm', mp ] )
			prev_mp = mp
	
	if udp_in != null and udp_in.is_listening():
		while( udp_in.get_available_packet_count() > 0 ):
			var from = udp_in.get_packet_ip() + ":" + str(udp_in.get_packet_port())
			print_line( "packet from "+ from )
			parse_var( udp_in.get_var() )

func _exit_tree():
	# closing sockets
	if udp_out != null:
		udp_out.close()
	if udp_in != null:
		udp_in.close()

func _out_connect():
	network_out( $out_ip.text, $out_port.text )

func _in_connect():
	network_in( $in_port.text )

func _on_swap():
	var tmp = $out_port.text
	$out_port.text = $in_port.text
	$in_port.text = tmp
